import React from 'react';
import {storiesOf} from '@storybook/react-native';
import ListHeading from 'poccketapp/src/components/ListHeading';
import CenterDecorator from '../../decorators';

storiesOf('List Heading Item', module)
  .addDecorator(CenterDecorator)
  .add('Sample #1', () => <ListHeading title={'Employment status'} />)
  .add('Sample #2', () => <ListHeading title={'L'} />);
