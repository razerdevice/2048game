import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {View, Alert} from 'react-native';
import Button from 'poccketapp/src/components/Button';
import CenterDecorator from './../../decorators';

const Container = ({children}) => (
  <View style={{marginVertical: 20}}>{children}</View>
);

storiesOf('Buttons', module)
  .addDecorator(CenterDecorator)
  .add('All types', () => (
    <React.Fragment>
      <Container>
        <Button color="danger" size="large" label="PRESS ME" />
      </Container>
      <Container>
        <Button
          color="primary"
          label="Got it"
          onPress={() => Alert.alert('Button', 'pressed')}
        />
      </Container>
      <Container>
        <Button color="neutral" size="small" label="Cancel" />
      </Container>
      <Container>
        <Button color="inverted" label="Add to Associates" outline />
      </Container>
      <Container>
        <Button color="inverted" label="Got it" />
      </Container>
    </React.Fragment>
  ))
  .add('Primary large', () => (
    <Button color="primary" size="large" label="Got it" />
  ))
  .add('Danger medium', () => (
    <Button color="danger" size="medium" label="Delete" />
  ))
  .add('Neutral small', () => (
    <Button color="neutral" size="small" label="Cancel" />
  ))
  .add('Outline inverted', () => (
    <Button color="inverted" size="medium" label="Add to Associates" outline />
  ))
  .add('Inverted', () => (
    <Button color="inverted" size="medium" label="GET STARTED" />
  ));
