import React from 'react';
import {storiesOf} from '@storybook/react-native';
import Message from 'poccketapp/src/components/Message';
import {CustomStatusBar, Padded} from './../../decorators';
import {View, ScrollView} from 'react-native';

const avatar =
  'https://cdn2.stylecraze.com/wp-content/uploads/2014/09/5-Perfect-Eyebrow-Shapes-For-Heart-Shaped-Face-1.jpg';
const link = 'https://pitchfork.com/features/cover-story/bon-iver-interview/';
const messageImage = 'https://i.ytimg.com/vi/ak6rI-j07QU/maxresdefault.jpg';

const welcome = 'Welcome to poccket and thank you for signing up!';

storiesOf('Messages', module)
  .addDecorator(Padded)
  .addDecorator(CustomStatusBar)
  .add('Sent default', () => <Message text="Hello there!" />)
  .add('Received w/ avatar & image', () => (
    <Message text={welcome} received avatar={avatar} image={messageImage} />
  ))
  .add('Sent w/ image', () => (
    <Message
      text="Do you recognize this beautiful scene?"
      image={messageImage}
    />
  ))
  .add('Sent w/ link', () => (
    <Message link={link} text="Check this article, it may inspire you." />
  ))
  .add('Combined', () => (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Message text={welcome} received avatar={avatar} />
      <Message text="Thank you!" />
      <Message text="Have any fresh ideas?" received />
      <Message
        text="Do you recognize this beautiful scene?"
        image={messageImage}
      />
      <Message received text="Let me think, maybe from Stalker?" />
      <Message link={link} text="Check this article, it may inspire you." />

      <View style={{height: 40}} />
    </ScrollView>
  ));
