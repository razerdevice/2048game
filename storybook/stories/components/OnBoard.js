import React from 'react';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import OnBoard from 'poccketapp/src/components/OnBoard';

storiesOf('Onboarding', module).add('On board start', () => (
  <View style={{flex: 1}}>
    <OnBoard />
  </View>
));
