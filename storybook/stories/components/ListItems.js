import React, {Fragment} from 'react';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import ListItem from 'poccketapp/src/components/ListItem';
import {palette} from '../../../src/theme/variables';
import CenterDecorator, {Divider} from '../../decorators';
import Assets from '../../../src/Assets';
import Card from '../../../src/components/Card';
import MyText from '../../../src/components/MyText';
import {cardInitial} from '../../testPropsData';

storiesOf('List Items', module)
  .addDecorator(CenterDecorator)
  .add('Basic', () => (
    <ListItem source={Assets.ENVELOPE} title={'Public Account'} />
  ))
  .add('Basic swipeable', () => (
    <ListItem source={Assets.ENVELOPE} title={'Public Account'} swipeable />
  ))
  .add('Basic w/ background color', () => (
    <ListItem
      source={Assets.ENVELOPE}
      title="Employment status"
      backgroundColor="#EEF9FA"
    />
  ))
  .add('Single line text', () => (
    <ListItem
      source={Assets.ENVELOPE}
      title="About Poccket"
      customColumn={<Column />}
    />
  ))
  .add('Double line text', () => (
    <ListItem
      source={Assets.ENVELOPE}
      title="Company"
      text={
        'You: This promises to be a sensational event. Expecting to see you there. Bring your friends too.'
      }
      customColumn={<Column />}
    />
  ))
  .add('Multi row #1', () => (
    <Fragment>
      <Divider />
      <ListItem
        source={{
          uri:
            'https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png',
        }}
        title="Grace Lim"
        customColumn={<Column />}
        backgroundColor="#EEF9FA"
        secondRow={
          <Card
            editStep={2}
            isEdit={false}
            onTextChange={() => null}
            backgroundColor="#CF251C"
            card={cardInitial}
          />
        }
      />
      <Divider />
    </Fragment>
  ))
  .add('Multi row #2', () => (
    <Fragment>
      <Divider />
      <ListItem
        source={{
          uri:
            'https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png',
        }}
        title={'Brooke Matthews'}
        customColumn={<MyText>45m</MyText>}
        secondRow={<TextRow />}
      />
      <Divider />
    </Fragment>
  ));

const Column = () => (
  <View
    style={{
      height: 13,
      width: 13,
      backgroundColor: palette.aqua,
      borderRadius: 10,
    }}
  />
);

const TextRow = () => (
  <View style={{padding: 10}}>
    <MyText style={{fontSize: 16}}>
      True, but I prefer their chicken nuggets!
    </MyText>
  </View>
);
