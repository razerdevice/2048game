import React from 'react';
import {storiesOf} from '@storybook/react-native';
import JumpToLetter from 'poccketapp/src/components/JumpToLetter';
import CenterDecorator from '../../decorators';

storiesOf('Jump To Letter', module)
  .addDecorator(CenterDecorator)
  .add('Jump To Letter', () => <JumpToLetter />);
