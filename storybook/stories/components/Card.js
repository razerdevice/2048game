import React from 'react';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import Card from 'poccketapp/src/components/Card';
import CenterDecorator from '../../decorators';
import {
  cardInitial,
  cardInitialBackImg,
  cardInitialEdit,
} from '../../testPropsData';

class CardStory extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      card: props.card,
    };
  }

  onTextChange = (field, value, subField) => {
    if (subField) {
      const newValue = {...this.state.card[field], [subField]: value};
      const newCard = {...this.state.card, [field]: newValue};
      return this.setState({card: newCard});
    }
    this.setState({card: {...this.state.card, [field]: value}});
  };

  render() {
    const {card} = this.state;
    const {editStep, isEdit, backgroundColor} = this.props;
    return (
      <View>
        <Card
          backgroundColor={backgroundColor}
          editStep={editStep}
          isEdit={isEdit}
          card={card}
          onTextChange={(field, value, subField) =>
            this.onTextChange(field, value, subField)
          }
        />
      </View>
    );
  }
}

storiesOf('Card', module)
  .addDecorator(CenterDecorator)
  .add('Edit mode', () => (
    <CardStory card={cardInitialEdit} isEdit={true} editStep={1} />
  ))
  .add('Edit mode w/ background', () => (
    <CardStory
      card={cardInitialEdit}
      isEdit={true}
      backgroundColor="#CF251C"
      editStep={1}
    />
  ))
  .add('View mode', () => (
    <CardStory card={cardInitial} isEdit={false} backgroundColor="#CF251C" />
  ))
  .add('View mode w/ back image', () => (
    <CardStory
      card={cardInitialBackImg}
      isEdit={false}
      backgroundColor="#CF251C"
    />
  ));
