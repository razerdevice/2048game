import React from 'react';
import {View, Image, Button as RNButton} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import ActionSheet from 'poccketapp/src/components/ActionSheet';
import Assets from 'poccketapp/src/Assets';
import Button from 'poccketapp/src/components/Button';
import CenterDecorator from '../../decorators';
import ListItem from 'poccketapp/src/components/ListItem';

const ActionSheetStory = ({actionsList, title}) => {
  const [visible, toggleModal] = React.useState(false);
  return (
    <View>
      <RNButton
        title="Show Action Sheet"
        onPress={() => toggleModal(!visible)}
      />
      <ActionSheet
        visible={visible}
        toggleModal={() => toggleModal(!visible)}
        title={title}
        actionsList={actionsList}
      />
    </View>
  );
};

storiesOf('Action Sheet', module)
  .addDecorator(CenterDecorator)
  .add('Delete', () => (
    <ActionSheetStory title="Delete Post?" actionsList={actionsDelete} />
  ));

storiesOf('Action Sheet', module)
  .addDecorator(CenterDecorator)
  .add('Actions', () => (
    <ActionSheetStory title="Actions" actionsList={actionsList} />
  ));

const actionsList = [
  <ListItem
    source={Assets.ENVELOPE}
    title={'Edit Post'}
    customColumn={<Image source={Assets.RIGHT_ARROW} />}
    onPress={() => console.log('Post edited')}
  />,
  <ListItem
    source={Assets.ENVELOPE}
    title={'Delete Post'}
    customColumn={<Image source={Assets.RIGHT_ARROW} />}
  />,
];

const actionsDelete = (
  <Button
    color="danger"
    size="fullWidth"
    label="Delete"
    onPress={() => console.log('Deleted')}
  />
);
