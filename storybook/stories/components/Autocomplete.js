import React from 'react';
import {storiesOf} from '@storybook/react-native';
import Autocomplete from 'poccketapp/src/components/Autocomplete';
import {CustomStatusBar} from '../../decorators';
import {contacts} from '../../testPropsData';

const AutocompleteStory = () => {
  const [selected, setSelected] = React.useState([]);

  return (
    <Autocomplete
      allItems={contacts}
      selected={selected}
      setSelected={setSelected}
    />
  );
};

storiesOf('Autocomplete', module)
  .addDecorator(CustomStatusBar)
  .add('Default', () => <AutocompleteStory />);
