import React from 'react';
import {storiesOf} from '@storybook/react-native';
import CenterDecorator, {Divider, CustomStatusBar} from './../../decorators';
import MyInput from 'poccketapp/src/components/MyInput';
import userIcon from 'poccketapp/assets/user.png';
import envelope from 'poccketapp/assets/envelope.png';
import lock from 'poccketapp/assets/lock.png';

class InputsStory extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      email: '',
      password: '',
    };
  }
  onTextChange = (field, value) => {
    this.setState({...this.state.card, [field]: value});
  };

  render() {
    return (
      <React.Fragment>
        <MyInput
          value={this.state.name}
          onChangeText={text => this.onTextChange('name', text)}
          source={userIcon}
          placeholder="Full Name"
        />
        <Divider />
        <MyInput
          value={this.state.email}
          onChangeText={text => this.onTextChange('email', text)}
          source={envelope}
          placeholder="Email"
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Divider />
        <MyInput
          value={this.state.password}
          onChangeText={text => this.onTextChange('password', text)}
          source={lock}
          placeholder="Password"
          secureTextEntry
        />
        <Divider />
      </React.Fragment>
    );
  }
}

const NameInputStory = () => {
  const [value, onChange] = React.useState('');
  return (
    <MyInput
      value={value}
      source={userIcon}
      placeholder="Full Name"
      onChangeText={text => onChange(text)}
    />
  );
};

const EmailInputStory = () => {
  const [value, onChange] = React.useState('');
  return (
    <MyInput
      value={value}
      source={envelope}
      placeholder="Email"
      keyboardType="email-address"
      autoCapitalize="none"
      onChangeText={text => onChange(text)}
    />
  );
};

const PasswordInputStory = () => {
  const [value, onChange] = React.useState('');
  return (
    <MyInput
      value={value}
      source={lock}
      placeholder="Password"
      secureTextEntry
      onChangeText={text => onChange(text)}
    />
  );
};

storiesOf('Input', module)
  .addDecorator(CustomStatusBar)
  .add('All inputs', () => <InputsStory />);

storiesOf('Input', module)
  .addDecorator(CenterDecorator)
  .addDecorator(CustomStatusBar)
  .add('Name input', () => <NameInputStory />)
  .add('Email input', () => <EmailInputStory />)
  .add('Password input', () => <PasswordInputStory />);
