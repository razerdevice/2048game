import React from 'react';
import {storiesOf} from '@storybook/react-native';
import CameraRoll from 'poccketapp/src/components/CameraRoll';
import {View, StyleSheet, SafeAreaView, Dimensions} from 'react-native';
import {palette} from 'poccketapp/src/theme/variables';
import MyText from 'poccketapp/src/components/MyText';
import {CustomStatusBar} from '../../decorators';
import MediaItem from 'poccketapp/src/components/MediaItem';
import {modArray, isLast} from 'poccketapp/src/Utils';
import {setSelectedPhotos} from 'poccketapp/src/actions/actionCreators';
import {useDispatch, useSelector} from 'react-redux';

const {width} = Dimensions.get('window');
const NUM_COLUMNS = width > 360 ? 4 : 3;

const VerticalCameraRoll = () => {
  const {selected} = useSelector(state => state.cameraRollReducer);
  const dispatch = useDispatch();

  const select = uri => {
    const newSelection = modArray(selected, uri);
    dispatch(setSelectedPhotos(newSelection));
  };
  const renderSeparator = () => <View style={styles.separator} />;

  const lastItem = [styles.item, styles.lastItem];
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.header}>
        <MyText style={styles.headerText}>CAMERA ROLL</MyText>
      </View>
      <CameraRoll
        numColumns={NUM_COLUMNS}
        limit={4}
        style={{flex: 1}}
        extraData={selected}
        ItemSeparatorComponent={renderSeparator}
        renderItem={({item, index}) => (
          <MediaItem
            image={item.node.image}
            onPress={uri => select(uri)}
            imageStyle={styles.image}
            itemStyle={isLast(index, NUM_COLUMNS) ? lastItem : styles.item}
            isSelected={selected.includes(item.node.image.uri)}
          />
        )}
      />
    </SafeAreaView>
  );
};

storiesOf('Vertical Camera Roll', module)
  .addDecorator(CustomStatusBar)
  .add('Default', () => <VerticalCameraRoll />);

const styles = StyleSheet.create({
  header: {
    height: 30,
    backgroundColor: palette.aqua,
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
  },
  item: {
    marginRight: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: width / NUM_COLUMNS - 1,
    height: width / NUM_COLUMNS - 1,
  },
  lastItem: {
    marginRight: 0,
    width: width / NUM_COLUMNS,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  separator: {
    height: 1,
    width: '100%',
  },
});
