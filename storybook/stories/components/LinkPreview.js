import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {View, StyleSheet} from 'react-native';
import LinkPreview from '../../../src/components/LinkPreview';
import CenterDecorator from '../../decorators';

storiesOf('Link Preview', module)
  .addDecorator(CenterDecorator)
  .add('View mode', () => (
    <View style={styles.container}>
      <LinkPreview
        link={
          'https://www.theverge.com/2019/9/16/20868071/huawei-mate-30-pro-lite-leaks-promotional-images-evan-blass-renders'
        }
      />
    </View>
  ));

storiesOf('Link Preview', module)
  .addDecorator(CenterDecorator)
  .add('Edit mode', () => (
    <View style={styles.container}>
      <LinkPreview
        editMode
        link={
          'https://www.theguardian.com/politics/2019/sep/16/raab-accuses-eu-of-political-posturing-before-johnson-talks'
        }
      />
    </View>
  ));

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    width: '100%',
  },
});
