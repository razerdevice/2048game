import React, {useState} from 'react';
import {storiesOf} from '@storybook/react-native';
import SearchInput from 'poccketapp/src/components/SearchInput';
import {CustomStatusBar} from '../../decorators';

const InputStory = ({withFilter}) => {
  const [search, setSearch] = useState('');

  return (
    <SearchInput
      value={search}
      onChangeText={text => setSearch(text)}
      placeholder="Search for new associates"
      withFilter={withFilter}
    />
  );
};

storiesOf('Search Input', module)
  .addDecorator(CustomStatusBar)
  .add('Default', () => <InputStory />)
  .add('With filter', () => <InputStory withFilter />);
