import React from 'react';
import {View, Button} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import AlertModal from 'poccketapp/src/components/AlertModal';
import CenterDecorator from '../../decorators';
import Strings from 'poccketapp/src/Strings';

const AlertModalStory = () => {
  const [visible, toggleModal] = React.useState(false);
  return (
    <View>
      <Button title="Show Alert Modal" onPress={() => toggleModal(!visible)} />
      <AlertModal
        visible={visible}
        toggleModal={() => toggleModal(!visible)}
        title={Strings.MODAL_TITLE}
        message={Strings.MODAL_TITLE}
      />
    </View>
  );
};

storiesOf('Alert Modal', module)
  .addDecorator(CenterDecorator)
  .add('Default', () => <AlertModalStory />);
