import React from 'react';
import {storiesOf} from '@storybook/react-native';
import CameraRoll from 'poccketapp/src/components/CameraRoll';
import {Dimensions, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Assets from 'poccketapp/src/Assets';
import {palette} from 'poccketapp/src/theme/variables';
import CenterDecorator from '../../decorators';
import MediaItem from 'poccketapp/src/components/MediaItem';

const {height, width} = Dimensions.get('window');

const MediaHead = ({onPress}) => {
  const headStyle = [styles.item, {backgroundColor: palette.aqua}];
  return (
    <TouchableOpacity onPress={() => !!onPress && onPress()} style={headStyle}>
      <Image style={{resizeMode: 'center'}} source={Assets.CARD_LABEL_FRONT} />
    </TouchableOpacity>
  );
};

storiesOf('Horizontal Camera Roll', module)
  .addDecorator(CenterDecorator)
  .add('Default', () => (
    <CameraRoll
      showsHorizontalScrollIndicator={false}
      style={styles.container}
      limit={12}
      horizontal
      ListHeaderComponent={<MediaHead />}
      renderItem={({item}) => (
        <MediaItem
          image={item.node.image}
          onPress={() => console.log('pressed on item')}
          itemStyle={styles.item}
          imageStyle={styles.image}
        />
      )}
    />
  ));

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: palette.lightGray,
    paddingHorizontal: 8,
    backgroundColor: 'white',
    paddingTop: 20,
    paddingBottom: 10,
  },
  text: {
    fontSize: 16,
    color: palette.darkGrey,
  },
  item: {
    width: width * 0.2,
    height: width * 0.2,
    margin: 7.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: width * 0.03,
  },
  image: {
    width: width * 0.2,
    height: width * 0.2,
    resizeMode: 'cover',
    borderRadius: width * 0.03,
  },
});
