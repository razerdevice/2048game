import React from 'react';
import {storiesOf} from '@storybook/react-native';
import RoundImage from 'poccketapp/src/components/RoundImage';
import CenterDecorator from './../../decorators';

const uri =
  'https://cdn2.stylecraze.com/wp-content/uploads/2014/09/5-Perfect-Eyebrow-Shapes-For-Heart-Shaped-Face-1.jpg';

storiesOf('Round Images', module)
  .addDecorator(CenterDecorator)
  .add('Default', () => <RoundImage uri={uri} />)
  .add('Large outline', () => <RoundImage size="large" outline uri={uri} />)
  .add('Small', () => <RoundImage size="small" uri={uri} />);
