import React from 'react';
import {storiesOf} from '@storybook/react-native';
import TabBar from '../../../src/components/TabBar';
import {palette} from 'poccketapp/src/theme/variables';
import CenterDecorator from '../../decorators';
import Assets from 'poccketapp/src/Assets';

const TabBarStory = () => {
  const [selected, setSelected] = React.useState(0);
  return (
    <TabBar
      activeColor={palette.aqua}
      color={palette.grey}
      selected={selected}
      setSelected={setSelected}>
      {[
        {title: 'One', image: Assets.CARD_LABEL_FRONT},
        {title: 'Two', image: Assets.MAIL},
        {title: 'Three', image: Assets.GEO},
      ]}
    </TabBar>
  );
};

const TabBarStorySecond = () => {
  const [selected, setSelected] = React.useState(0);
  return (
    <TabBar
      activeColor={palette.red}
      color={palette.grey}
      selected={selected}
      setSelected={setSelected}>
      {[
        {title: 'One', image: Assets.HEART, imageActive: Assets.HEART_RED},
        {title: 'Two', image: Assets.MESSAGE, imageActive: Assets.MESSAGE_RED},
      ]}
    </TabBar>
  );
};

storiesOf('Tab Bar', module)
  .addDecorator(CenterDecorator)
  .add('2 Columns', () => <TabBarStorySecond />);

storiesOf('Tab Bar', module)
  .addDecorator(CenterDecorator)
  .add('3 Columns', () => <TabBarStory />);
