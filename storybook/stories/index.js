import React from 'react';
import {addDecorator} from '@storybook/react-native';
import {withKnobs} from '@storybook/addon-knobs';

// register global decorator
addDecorator(withKnobs);

// import all stories here
require('./components/Button');
require('./components/OnBoard');
require('./components/Input');
require('./components/AlertModal');
require('./components/Card');
require('./components/JumpToLetter');
require('./components/ListHeading');
require('./components/ListItems');
require('./components/ActionSheet');
require('./components/SearchInput');
require('./components/Autocomplete');
require('./components/HCameraRoll');
require('./components/VCameraRoll');
require('./components/TabBar');
require('./components/LinkPreview');
require('./components/RoundImage');
require('./components/Messages');
