import React, {Fragment} from 'react';
import {Platform, StatusBar, View} from 'react-native';
import {palette} from '../src/theme/variables';
import {isIphoneX} from '../src/Utils';

const STATUS_BAR_HEIGHT = isIphoneX() ? 45 : 20;

const CenterDecorator = storyFn => (
  <View style={styles.center}>{storyFn()}</View>
);

export const Padded = storyFn => (
  <View style={[styles.center, {paddingHorizontal: 15}]}>{storyFn()}</View>
);

export const CustomStatusBar = storyFn => (
  <Fragment>
    {Platform.OS === 'android' ? (
      <StatusBar backgroundColor={palette.aqua} />
    ) : (
      <View
        style={{backgroundColor: palette.aqua, height: STATUS_BAR_HEIGHT}}
      />
    )}
    {storyFn()}
  </Fragment>
);

export const Divider = () => <View style={styles.divider}></View>;

const styles = {
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: palette.lightGrey,
    marginHorizontal: 5,
  },
  header: {
    height: 30,
    backgroundColor: palette.aqua,
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
  },
};

export default CenterDecorator;
