export const cardInitialEdit = {
  name: '',
  occupation: '',
  email: '',
  phone: '',
  website: '',
  companyName: '',
  location: {
    name: '',
    lat: '',
    lng: '',
  },
  front: {
    photo: {
      thumb: '',
      medium: '',
      large:
        'https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png',
    },
    background: '#0798AF',
    text: '#F3F3F3',
  },
  back: {
    photo: {
      thumb: '',
      medium: '',
      large: '',
    },
    background: '#F3F3F3',
    text: '#F3F3F3',
  },
};

export const cardInitial = {
  name: 'John Doe',
  occupation: '',
  email: 'john.doe@plycode.com',
  phone: '+12345678',
  website: 'plycode.com',
  companyName: 'PLYCODE',
  location: {
    name: 'Sunset blvd.',
    lat: '',
    lng: '',
  },
  front: {
    photo: {
      thumb: '',
      medium: '',
      large:
        'https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png',
    },
    background: '#0798AF',
    text: '#F3F3F3',
  },
  back: {
    photo: {
      thumb: '',
      medium: '',
      large: '',
    },
    background: '#F3F3F3',
    text: '#F3F3F3',
  },
};

export const cardInitialBackImg = {
  name: '',
  occupation: '',
  email: 'username@gmail.com',
  phone: '+12345678',
  website: 'google.com',
  companyName: 'Private company',
  location: {
    name: 'Sunset blvd.',
    lat: '',
    lng: '',
  },
  front: {
    photo: {
      thumb: '',
      medium: '',
      large:
        'https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png',
    },
    background: '#0798AF',
    text: '#F3F3F3',
  },
  back: {
    photo: {
      thumb: '',
      medium: '',
      large:
        'https://purepng.com/public/uploads/large/purepng.com-mario-runningmariofictional-charactervideo-gamefranchisenintendodesigner-1701528632710brm3o.png',
    },
    background: '#F3F3F3',
    text: '#F3F3F3',
  },
};

export const contacts = [
  {
    id: 1,
    title: 'Brian Williams',
  },
  {
    id: 2,
    title: 'Brooke Matthews',
  },
  {
    id: 3,
    title: 'Jack White',
  },
  {
    id: 4,
    title: 'Robert Smith',
  },
  {
    id: 5,
    title: 'Jimmy Page',
  },
  {
    id: 6,
    title: 'Justin Vernon',
  },
  {
    id: 7,
    title: 'James Blake',
  },
];

export const images = [
  {
    id: 1,
    url:
      'https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/tnc_48980557.jpg',
  },
  {
    id: 2,
    url:
      'https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/tnc_48980557.jpg',
  },
  {
    id: 3,
    url:
      'https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/tnc_48980557.jpg',
  },
  {
    id: 4,
    url:
      'https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/tnc_48980557.jpg',
  },
  {
    id: 5,
    url:
      'https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/tnc_48980557.jpg',
  },
  {
    id: 6,
    url:
      'https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/tnc_48980557.jpg',
  },
  {
    id: 7,
    url: 'https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg',
  },
];
