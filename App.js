import React from 'react';
import {StyleSheet} from 'react-native';
import {Scene, Router, Stack} from 'react-native-router-flux';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Home from 'poccketapp/src/screens/Home';
import Storybook from 'poccketapp/src/screens/Storybook';
import Game from 'poccketapp/src/screens/Game';

const App = () => (
  <Router sceneStyle={styles.root}>
    <Stack key="root">
      <Scene key="Home" hideNavBar={true} component={Home} />
      <Scene key="Storybook" hideNavBar={true} component={Storybook} />
      <Scene key="Game" title="2048" component={Game} />
    </Stack>
  </Router>
);

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;
