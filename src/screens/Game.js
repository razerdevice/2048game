import React, {Component} from 'react';
import {View, StyleSheet, Dimensions, Animated, Easing} from 'react-native';
import {connect} from 'react-redux';
import {updateGrid} from '../actions/actionCreators';
import {palette} from '../theme/variables';
import MyText from '../components/MyText';
import Toast from '../components/Toast';
import moveGrid from '../grid/Grid';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import _ from 'lodash';

const {SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
const {width} = Dimensions.get('window');
const TILE_WIDTH = (width * 0.9 - 20) / 4;

class Game extends Component {
  state = {
    metadata: null,
    horizontal: null,
    newValueCoordinates: null,
    move: new Animated.Value(0),
    opacity: new Animated.Value(0),
  };
  componentDidMount() {
    this.toastRef.animateToast('Swipe on grid to move tiles');
  }

  move = direction => {
    this.toastRef.animateToast(direction);
    const {grid, metadata, newValueCoordinates} = moveGrid(
      _.cloneDeep(this.props.grid),
      direction,
    );
    const horizontal = direction === SWIPE_LEFT || direction === SWIPE_RIGHT;
    this.setState({metadata, horizontal}, () => {
      this.moveTile([...grid], newValueCoordinates);
    });
  };

  renderGrid = () => {
    const {grid} = this.props;
    return grid.map((row, rowIdx) => this.renderRow(row, rowIdx));
  };

  moveTile = (grid, newValueCoordinates) => {

    const {updateGrid} = this.props;
    const {move} = this.state;
    move.setValue(0);
    Animated.timing(move, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start(() => {
      this.setState({metadata: null, newValueCoordinates});
      updateGrid(grid);
      this.animateNewValue();
    });
  };

  animateNewValue = () => {
    const {opacity} = this.state;
    opacity.setValue(0);
    Animated.timing(opacity, {toValue: 1, duration: 700}).start();
  };

  renderRow = (row, rowIdx) => {
    const {opacity} = this.state;
    return row.map((value, index) => {
      const {left, top} = this.getPosition(index, rowIdx);
      let tileStyle = [styles.tile, {left, top}];

      const {newValueCoordinates} = this.state;
      if (newValueCoordinates) {
        const [x, y] = newValueCoordinates;
        if (x === rowIdx && y === index) {
          tileStyle = [...tileStyle, {opacity}];
        }
      }
      const tileBody = [styles.tileBody, {backgroundColor: palette[value]}];
      return (
        value && (
          <Animated.View style={tileStyle} key={`${index + Date.now()}`}>
            <View style={tileBody}>
              <MyText style={styles.tileNumber}>{value}</MyText>
            </View>
          </Animated.View>
        )
      );
    });
  };

  getPosition = (index, rowIdx) => {
    const {horizontal, metadata} = this.state;
    const staticLeft = index * TILE_WIDTH;
    const staticTop = rowIdx * TILE_WIDTH;
    if (!metadata) {
      return {top: staticTop, left: staticLeft};
    }
    const {moveDistance} = horizontal
      ? this.interpolate(index, rowIdx)
      : this.interpolate(rowIdx, index);

    const left = horizontal ? moveDistance : staticLeft;
    const top = horizontal ? staticTop : moveDistance;
    return {top, left};
  };

  interpolate = (xIdx, yIdx) => {
    const {metadata, move} = this.state;
    const staticIndex = xIdx * TILE_WIDTH;
    // Find tile in metadata array and checks that tile index have changed
    const newIndex =
      metadata &&
      metadata[yIdx].find(
        ({initialIdx, idx}) => initialIdx === xIdx && idx !== xIdx,
      );
    if (!newIndex) return {moveDistance: staticIndex, opacity: 1};

    const moveDistance = move.interpolate({
      inputRange: [0, 1],
      outputRange: [TILE_WIDTH * xIdx, TILE_WIDTH * newIndex.idx],
    });
    return {moveDistance};
  };

  onSwipe = (gestureName, gestureState) => {
    return gestureName && this.move(gestureName);
  };

  render() {
    const config = {
      velocityThreshold: 0.1,
      directionalOffsetThreshold: 200,
      gestureIsClickThreshold: 1,
    };
    return (
      <View style={styles.container}>
        <GestureRecognizer
          style={styles.field}
          onSwipe={(direction, state) => this.onSwipe(direction, state)}
          config={config}>
          <View style={styles.fieldInner}>{this.renderGrid()}</View>
        </GestureRecognizer>
        <Toast ref={el => (this.toastRef = el)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.sand,
  },
  field: {
    width: width * 0.9,
    height: width * 0.9,
    borderRadius: 15,
    backgroundColor: palette.clay,
    padding: 10,
  },
  fieldInner: {
    flex: 1,
  },
  tile: {
    width: TILE_WIDTH,
    aspectRatio: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1,
  },
  tileBody: {
    width: TILE_WIDTH * 0.9,
    aspectRatio: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tileNumber: {
    fontSize: 34,
    color: palette.tileNumber,
    fontWeight: '700',
  },
  appName: {
    color: palette.aqua,
    fontSize: 20,
    fontWeight: '400',
    padding: 20,
  },
  controlText: {
    fontSize: 20,
    marginVertical: 10,
    color: palette.tileNumber,
    textAlign: 'center',
  },
});

const mapStateToProps = state => {
  const {grid} = state.mainReducer;
  return {grid};
};

const mapDispatchToProps = dispatch => ({
  updateGrid: data => dispatch(updateGrid(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Game);
