export default {
  ONBOARD_1: 'Easily create, share and manage your own Poccket cards',
  ONBOARD_2: 'Add Associates and see their latest networking activity',
  ONBOARD_3:
    'Search the directory to find associates and expand your professional network',
  CARD_FRONT_1: 'Tap to Add Front Cover',
  CARD_FRONT_2: 'Tap to Add Logo to Front Cover',
  MODAL_TITLE: 'Done!',
  MODAL_MESSAGE: 'A Reset Password email has been successfully sent to you.',
};
