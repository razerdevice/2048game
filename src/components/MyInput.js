import React from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {palette} from './../theme/variables';
import eye from 'poccketapp/assets/eye.png';
import eyeOff from 'poccketapp/assets/eye-off.png';

const MyInput = props => {
  const {
    source,
    secureTextEntry,
    containerStyle,
    inputStyle,
    color,
    onChangeText,
    value,
    ...restProps
  } = props;

  const [isHidePassword, setHidePassword] = React.useState(true);

  return (
    <View style={containerStyle ? containerStyle : styles.inputContainer}>
      {source && (
        <View style={styles.imageView}>
          <Image source={source} />
        </View>
      )}
      <TextInput
        value={value}
        style={inputStyle ? inputStyle : styles.input}
        placeholderTextColor={color ? color : palette.lightGray}
        selectionColor={color ? palette.grey : palette.aqua}
        secureTextEntry={secureTextEntry && isHidePassword}
        onChangeText={text => onChangeText(text)}
        {...restProps}
      />
      {!!value && secureTextEntry && (
        <TouchableOpacity onPress={() => setHidePassword(!isHidePassword)}>
          <Image source={isHidePassword ? eye : eyeOff} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 70,
    borderColor: 'grey',
    paddingLeft: 13,
    paddingRight: 15,
  },
  input: {
    height: 40,
    flex: 1,
    marginLeft: 13,
    marginRight: 5,
    fontSize: 16,
    fontFamily: 'Helvetica Neue',
    // color: 'black',
  },
  imageView: {
    minWidth: 20,
    alignItems: 'center',
  },
});

export default MyInput;
