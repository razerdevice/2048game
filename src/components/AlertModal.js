import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {palette} from '../theme/variables';
import Button from './Button';
import MyText from './MyText';

const AlertModal = ({visible, toggleModal, title, message, ...restProps}) => {
  return (
    <Modal isVisible={visible} {...restProps}>
      <View style={styles.container}>
        <MyText style={{...styles.text, fontSize: 26}}>{title}</MyText>
        <MyText style={[styles.text, styles.message]}>{message}</MyText>
        <Button color="primary" label="Got it" onPress={toggleModal} />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    height: 190,
    width: 275,
    backgroundColor: 'white',
    borderRadius: 15,
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    textAlign: 'center',
  },
  message: {
    fontSize: 16,
    color: palette.grey,
    marginTop: -20,
  },
});

export default AlertModal;
