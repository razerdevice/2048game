import React from 'react';
import {View, StyleSheet} from 'react-native';
import {palette} from '../theme/variables';
import MyText from './MyText';

const ListHeading = ({title}) => {
  return (
    <View style={styles.container}>
      <MyText style={styles.text}>{title}</MyText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: palette.lightGrey,
    width: '100%',
    height: 40,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    color: palette.darkGrey,
  },
});

export default ListHeading;
