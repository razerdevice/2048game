import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import {palette} from '../theme/variables';
import MyText from './MyText';

const {height, width} = Dimensions.get('window');
const alphabet = 'ABC-DEFG-HIJKLM-NOPQRS-TUVW-XYZ';

const Letter = ({letter, selected, setSelected}) => {
  const isSelected = () => letter === selected;
  const style = isSelected()
    ? [styles.default, {backgroundColor: palette.aqua}]
    : styles.default;

  return (
    <TouchableOpacity onPress={() => setSelected(letter)} style={style}>
      <MyText style={isSelected() ? styles.selectedText : styles.defaultText}>
        {letter}
      </MyText>
    </TouchableOpacity>
  );
};

const JumpToLetter = ({}) => {
  const [selected, setSelected] = useState('A');

  const renderRow = letters => {
    const components = letters
      .split('')
      .map(letter => (
        <Letter
          key={letter}
          letter={letter}
          selected={selected}
          setSelected={setSelected}
        />
      ));
    return (
      <View style={styles.row} key={letters}>
        {components}
      </View>
    );
  };

  const rowsArr = alphabet.split('-').map(letters => renderRow(letters));
  return <View style={styles.container}>{rowsArr}</View>;
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',
    paddingVertical: height * 0.025,
  },
  default: {
    alignItems: 'center',
    justifyContent: 'center',
    height: width * 0.12,
    width: width * 0.12,
    borderRadius: 30,
    backgroundColor: palette.lightGrey,
    margin: 0.013 * width,
  },
  defaultText: {
    fontSize: 22,
    color: palette.grey,
    fontWeight: '700',
  },
  selectedText: {
    fontSize: 22,
    color: 'white',
    fontWeight: '700',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default JumpToLetter;
