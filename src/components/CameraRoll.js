import {getPhotos} from '@react-native-community/cameraroll';
import React from 'react';
import {View, FlatList, Platform, Alert, ActivityIndicator} from 'react-native';
import {permissionAndroid} from '../Utils';
import {palette} from '../theme/variables';
import {connect} from 'react-redux';
import {setPhotos, setIsFetching} from '../actions/actionCreators';

const Footer = () => (
  <View style={{padding: 20}}>
    <ActivityIndicator size="large" color={palette.aqua} />
  </View>
);

class CameraRoll extends React.Component {
  async componentDidMount() {
    if (Platform.OS === 'ios') return this.fetch();
    const response = await permissionAndroid();
    if (response === 'granted') {
      return this.fetch();
    }
    Alert.alert('Denied', 'Gallery permission denied');
  }

  fetch = async () => {
    const {endCursor, hasNext, photos, isFetching, limit} = this.props;
    const {setPhotos, setIsFetching} = this.props;
    try {
      if (!hasNext || isFetching) return;

      setIsFetching(true);
      const {edges, page_info} = await getPhotos({
        first: limit,
        after: endCursor,
        assetType: 'Photos',
      });
      setPhotos({
        photos: [...photos, ...edges],
        endCursor: page_info.end_cursor,
        hasNext: page_info.has_next_page,
      });
    } catch (e) {
      console.log(e);
    }
    setIsFetching(false);
  };

  render() {
    const {isFetching, photos, style, ...restProps} = this.props;
    return (
      <View style={style}>
        <FlatList
          data={photos}
          onEndReached={this.fetch}
          onEndReachedThreshold={0.5}
          keyExtractor={item => item.node.image.uri}
          {...restProps}
          ListFooterComponent={isFetching && <Footer />}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {
    selected,
    endCursor,
    hasNext,
    isFetching,
    photos,
  } = state.cameraRollReducer;
  return {selected, endCursor, hasNext, isFetching, photos};
};

const mapDispatchToProps = dispatch => ({
  setPhotos: data => dispatch(setPhotos(data)),
  setIsFetching: data => dispatch(setIsFetching(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CameraRoll);
