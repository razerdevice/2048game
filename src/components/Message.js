import React from 'react';
import propTypes from 'prop-types';
import urlPropType from 'url-prop-type';
import MyText from './MyText';
import LinkPreview from './LinkPreview';
import RoundImage from './RoundImage';
import {palette} from '../theme/variables';
import {Image, View, StyleSheet} from 'react-native';

const Message = ({text, received, avatar, image, link}) => {
  const {container, containerSent, message, messageSent} = styles;

  const renderPreview = () => (
    <LinkPreview link={link} style={{marginBottom: 5}} />
  );

  const containerStyle = received ? container : [container, containerSent];
  const rowStyle = received
    ? styles.row
    : [styles.row, {justifyContent: 'flex-end'}];
  const messageStyle = received ? message : [message, messageSent];
  const textStyle = received ? styles.text : [styles.text, {color: 'white'}];

  return (
    <View style={containerStyle}>
      {avatar && (
        <View style={styles.avatarContainer}>
          <RoundImage uri={avatar} size="small" />
        </View>
      )}
      <View style={styles.messageContainer}>
        {image && <Image style={styles.messageImage} source={{uri: image}} />}
        {link && renderPreview()}
        {!!text && (
          <View style={rowStyle}>
            <View style={messageStyle}>
              <MyText multiline={true} style={textStyle}>
                {text}
              </MyText>
            </View>
          </View>
        )}
      </View>
    </View>
  );
};

Message.propTypes = {
  text: propTypes.string,
  received: propTypes.bool,
  avatar: urlPropType,
  image: urlPropType,
  link: urlPropType,
};

export default Message;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
  },
  containerSent: {
    justifyContent: 'flex-end',
  },
  messageContainer: {
    maxWidth: '75%',
    marginTop: 14,
  },
  row: {
    flexDirection: 'row',
  },
  message: {
    borderRadius: 25,
    backgroundColor: palette.lightGrey,
    paddingVertical: 15,
    paddingHorizontal: 12,
  },
  messageSent: {
    backgroundColor: palette.aqua,
  },
  text: {
    fontSize: 16,
  },
  avatarContainer: {
    justifyContent: 'flex-end',
    marginRight: 10,
  },
  messageImage: {
    width: '100%',
    aspectRatio: 1,
    borderRadius: 15,
    resizeMode: 'cover',
    marginBottom: 5,
  },
});
