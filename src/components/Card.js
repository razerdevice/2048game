import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import Assets from '../Assets';
import {palette} from '../theme/variables';
import MyText from './MyText';
import Swiper from 'react-native-swiper';
import Dot from './SwiperDot';
import MyInput from './MyInput';
import Strings from '../Strings';

const {height, width} = Dimensions.get('window');
const isWide = () => width >= 360;

const Card = ({
  isEdit,
  backgroundColor,
  editStep,
  card,
  card: {front},
  card: {back},
  onTextChange,
}) => {
  const renderFront = () => {
    const {photo, background: frontBg} = front;

    let cardStyle = [styles.card, styles.cardFront];

    cardStyle = isWide() ? cardStyle : [...cardStyle, styles.cardSmall];

    if (isEdit) {
      return (
        <View style={cardStyle}>
          <TouchableOpacity>
            <Image source={Assets.CARD_LABEL_FRONT} />
          </TouchableOpacity>
          <MyText style={styles.frontText}>
            {editStep === 1 ? Strings.CARD_FRONT_1 : Strings.CARD_FRONT_2}
          </MyText>
        </View>
      );
    }
    cardStyle = frontBg
      ? [...cardStyle, {backgroundColor: frontBg}]
      : cardStyle;

    return (
      <View style={cardStyle}>
        <Image style={styles.image} source={{uri: photo.large}} />
      </View>
    );
  };

  const getIcon = property =>
    backgroundColor ? Assets[`${property}_L`] : Assets[`${property}`];

  const renderBack = () => {
    const {photo, text, background: rearBg} = back;

    const color = backgroundColor ? palette.white : palette.grey;
    const bgColor = backgroundColor ? backgroundColor : palette.lightGrey;
    let cardStyle = {...styles.card, backgroundColor: bgColor};
    cardStyle = isWide() ? cardStyle : [cardStyle, styles.cardSmall];

    return (
      <View style={cardStyle}>
        {photo.large ? (
          <Image style={styles.image} source={{uri: photo.large}} />
        ) : (
          <React.Fragment>
            <MyInput
              value={card.name}
              placeholder="Full Name"
              inputStyle={[styles.input, {color}, {fontWeight: 'bold'}]}
              color={color}
              containerStyle={styles.inputContainerTop}
              onChangeText={text => onTextChange('name', text)}
              editable={isEdit}
            />
            <MyInput
              value={card.occupation}
              placeholder="Occupation @ Company"
              inputStyle={[styles.input, {color}]}
              color={color}
              containerStyle={styles.inputContainerBottom}
              onChangeText={text => onTextChange('occupation', text)}
              editable={isEdit}
            />
            <MyInput
              value={card.email}
              placeholder="Email Address"
              inputStyle={[styles.inputSmall, {color}]}
              color={color}
              source={getIcon('MAIL')}
              containerStyle={styles.inputContainerSmall}
              onChangeText={text => onTextChange('email', text)}
              editable={isEdit}
            />
            <MyInput
              value={card.location.name}
              placeholder="Office Address"
              inputStyle={[styles.inputSmall, {color}]}
              color={color}
              source={getIcon('GEO')}
              containerStyle={styles.inputContainerSmall}
              onChangeText={text => onTextChange('location', text, 'name')}
              editable={isEdit}
            />
            <MyInput
              value={card.phone}
              placeholder="Phone Number"
              inputStyle={[styles.inputSmall, {color}]}
              color={color}
              source={getIcon('PHONE')}
              containerStyle={styles.inputContainerSmall}
              onChangeText={text => onTextChange('phone', text)}
              editable={isEdit}
            />
            <MyInput
              value={card.website}
              placeholder="Website"
              inputStyle={[styles.inputSmall, {color}]}
              color={color}
              source={getIcon('GLOBE')}
              containerStyle={styles.inputContainerSmall}
              onChangeText={text => onTextChange('website', text)}
              editable={isEdit}
            />
          </React.Fragment>
        )}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Swiper
        loop={false}
        dot={<Dot small color={palette.lightGrey} />}
        activeDot={<Dot small active />}>
        {renderFront()}
        {renderBack()}
      </Swiper>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    height: 260,
    justifyContent: 'center',
  },
  card: {
    alignSelf: 'center',
    width: 345,
    height: 210,
    borderRadius: 13,
    paddingVertical: height * 0.03,
    paddingHorizontal: 29,
  },
  cardSmall: {
    alignSelf: 'center',
    width: 300,
    height: 183,
    borderRadius: 10,
    paddingVertical: 5,
    paddingHorizontal: 18,
  },
  cardFront: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.lightGrey,
  },
  frontText: {
    marginTop: 15,
    fontSize: 18,
    color: palette.grey,
  },
  inputContainerTop: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 10,
    marginTop: 10,
  },
  inputContainerBottom: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputContainerSmall: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 28,
  },
  input: {
    height: 40,
    flex: 1,
    marginRight: 5,
    fontSize: 16,
    fontFamily: 'Helvetica Neue',
  },
  inputSmall: {
    height: 40,
    flex: 1,
    marginRight: 5,
    marginLeft: 7,
    fontSize: 12,
    fontFamily: 'Helvetica Neue',
  },
  image: {
    width: 280,
    height: 160,
    resizeMode: 'contain',
  },
});

export default Card;
