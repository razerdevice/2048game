import React, {Fragment} from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import Modal from 'react-native-modal';
import {Divider} from '../../storybook/decorators';
import MyText from './MyText';
import Button from './Button';
import {palette} from '../theme/variables';

const {height} = Dimensions.get('window');

const ActionSheet = ({toggleModal, title, actionsList, visible}) => {
  const renderList = () =>
    actionsList.map((component, index) => (
      <Fragment key={index}>
        <Divider />
        {component}
        {index === actionsList.length - 1 && <Divider />}
      </Fragment>
    ));

  return (
    <Modal isVisible={visible} style={styles.modal}>
      <View style={styles.container}>
        <View style={[styles.border, {left: -10}]} />
        <View style={[styles.border, {right: -10}]} />
        <MyText style={styles.text}>{title}</MyText>

        {Array.isArray(actionsList) ? renderList() : actionsList}

        <View style={styles.button}>
          <Button
            color="neutral"
            size="fullWidth"
            label="Cancel"
            onPress={toggleModal}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: 0,
  },
  container: {
    alignSelf: 'center',
    width: '100%',
    marginHorizontal: -30,
    backgroundColor: 'white',
    borderTopLeftRadius: 28,
    borderTopRightRadius: 28,
    alignItems: 'center',
  },
  border: {
    height: 65,
    width: 65,
    position: 'absolute',
    borderRadius: 35,
    top: 1,
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: height * 0.02,
    color: palette.coalGrey,
  },
  button: {
    marginVertical: height * 0.03,
  },
});

export default ActionSheet;
