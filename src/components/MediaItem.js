import React from 'react';
import {
  ImageBackground,
  Image,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import {palette} from '../theme/variables';
import Assets from '../Assets';

const CheckMarker = () => (
  <View style={styles.marker}>
    <Image source={Assets.CHECK_MARK_WHITE} style={styles.markerImage} />
  </View>
);

const MediaItem = ({
  isSelected,
  onPress,
  itemStyle,
  imageStyle,
  image: {uri},
}) => {
  return (
    <TouchableOpacity
      onPress={() => !!onPress && onPress(uri)}
      style={isSelected ? [itemStyle, styles.selected] : itemStyle}>
      <ImageBackground
        style={imageStyle}
        source={{uri}}
        imageStyle={imageStyle}>
        {isSelected && <View style={styles.imageBg} />}
      </ImageBackground>
      {isSelected && <CheckMarker />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  selected: {
    borderWidth: 2,
    borderColor: palette.aqua,
  },
  marker: {
    position: 'absolute',
    bottom: 8,
    right: 8,
    height: 20,
    width: 20,
    backgroundColor: palette.aqua,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  markerImage: {
    height: '80%',
    aspectRatio: 1,
  },
  imageBg: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
});

export default MediaItem;
