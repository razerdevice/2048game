import React from 'react';
import {
  View,
  StyleSheet,
  Animated,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {palette} from '../theme/variables';
const {width} = Dimensions.get('window');
import MyText from './MyText';

const Tab = ({
  onPress,
  isSelected,
  activeColor,
  color,
  content: {title, image, imageActive},
}) => {
  const getSource = () => {
    if (imageActive) {
      return isSelected ? imageActive : image;
    }
    return image;
  };
  const Touchable =
    Platform.OS === 'ios' ? TouchableOpacity : TouchableWithoutFeedback;
  return (
    <Touchable style={{flex: 1}} activeOpacity={0.6} onPress={() => onPress()}>
      <View style={styles.tab}>
        {image && (
          <Image source={getSource()} style={styles.image} fadeDuration={0} />
        )}
        {title && (
          <MyText style={isSelected ? {color: activeColor} : {color}}>
            {title}
          </MyText>
        )}
      </View>
    </Touchable>
  );
};

const TabBar = ({children, activeColor, color, selected, setSelected}) => {
  const tabWidth = width / children.length;
  let margin = new Animated.Value(0);

  const animateMargin = index => {
    const start = tabWidth * selected;
    const end = start + tabWidth * (index - selected);
    margin.setValue(start);
    Animated.timing(margin, {
      toValue: end,
      duration: 150,
    }).start(() => setSelected(index));
  };

  return (
    <React.Fragment>
      <View style={styles.container}>
        {children.map((content, index) => (
          <Tab
            onPress={() => animateMargin(index)}
            key={index}
            content={content}
            activeColor={activeColor}
            color={color}
            isSelected={index === selected}
          />
        ))}
      </View>
      <Animated.View
        style={[
          {...styles.underline, width: tabWidth},
          {marginLeft: margin, backgroundColor: activeColor},
        ]}
      />
      <View style={styles.border} />
    </React.Fragment>
  );
};

export default TabBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    height: 60,
    backgroundColor: 'white',
  },
  underline: {
    height: 2,
    backgroundColor: palette.aqua,
    alignSelf: 'flex-start',
    zIndex: 2,
  },
  border: {
    height: 1,
    backgroundColor: palette.grey,
    width: '100%',
    marginTop: -1,
  },
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    flexDirection: 'row',
  },
  image: {
    resizeMode: 'contain',
    width: '30%',
    height: '30%',
  },
});
