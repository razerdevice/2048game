import React, {Component} from 'react';
import {Animated, StyleSheet, Text, View} from 'react-native';

class Toast extends Component {
  toastOpacity = new Animated.Value(0);
  state = {
    text: '',
  };

  animateToast = text => {
    this.setState({text});
    this.toastOpacity.setValue(0);
    Animated.timing(this.toastOpacity, {toValue: 1, duration: 2500}).start();
  };

  render() {
    const opacity = this.toastOpacity.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0.6, 0.6, 0],
    });
    return (
      <View style={styles.absolute}>
        <Animated.View style={[styles.toast, {opacity}]}>
          <Text style={{color: 'white'}}>{this.state.text}</Text>
        </Animated.View>
      </View>
    );
  }
}

export default Toast;

const styles = StyleSheet.create({
  toast: {
    backgroundColor: 'black',
    borderRadius: 5,
    padding: 8,
  },
  absolute: {
    left: 0,
    right: 0,
    position: 'absolute',
    top: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
