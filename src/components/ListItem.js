import React, {Fragment} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  Animated,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {palette} from './../theme/variables';
import Assets from './../Assets';
import MyText from './MyText';
import Swipeable from 'react-native-gesture-handler/Swipeable';

const {width} = Dimensions.get('window');

const ListItem = props => {
  const {
    source,
    title,
    text,
    customColumn,
    secondRow,
    backgroundColor = 'white',
    onPress,
    id,
    swipeable,
  } = props;

  const renderColumn = () => (
    <TouchableOpacity style={styles.thirdCol}>{customColumn}</TouchableOpacity>
  );

  const renderMain = () => {
    return (
      <View style={styles.main}>
        <MyText numberOfLines={1} ellipsizeMode="tail" style={styles.title}>
          {title}
        </MyText>
        {text && (
          <MyText numberOfLines={1} ellipsizeMode="tail" style={styles.text}>
            {text}
          </MyText>
        )}
      </View>
    );
  };

  const onRemove = () => Alert.alert('Removed', 'Item deleted');

  const RightActions = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
    return (
      <TouchableOpacity onPress={onRemove} style={styles.rightAction}>
        <Animated.Image
          style={{transform: [{scale: scale}]}}
          source={Assets.TRASH_CAN}
        />
      </TouchableOpacity>
    );
  };
  const itemStyle = [styles.container, {backgroundColor}];

  return (
    <View style={{width: '100%', backgroundColor}}>
      <Swipeable
        renderRightActions={swipeable && RightActions}
        overshootRight={false}>
        <TouchableOpacity
          onPress={() => !!onPress && onPress(id)}
          style={itemStyle}>
          <View style={styles.imageView}>
            <Image style={styles.image} source={source} />
          </View>
          <View style={styles.content}>
            {renderMain()}
            {customColumn && renderColumn()}
          </View>
        </TouchableOpacity>
      </Swipeable>

      {secondRow && (
        <Fragment>
          <View style={{height: 5}} />
          <View>{secondRow}</View>
        </Fragment>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 70,
    borderColor: 'grey',
    paddingRight: 5,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  main: {
    maxWidth: width * 0.65,
  },
  thirdCol: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  imageView: {
    minWidth: 20,
    alignItems: 'center',
    paddingHorizontal: 13,
  },
  title: {
    color: palette.coalGrey,
    fontSize: 16,
    marginBottom: 3,
    fontWeight: '300',
  },
  text: {
    fontSize: 16,
    color: palette.grey,
  },
  rightAction: {
    paddingHorizontal: 10,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: palette.pink,
  },
  image: {
    width: 42,
    height: 42,
    resizeMode: 'contain',
  },
});

export default ListItem;
