import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {palette} from './../theme/variables';
import Assets from 'poccketapp/src/Assets';

const SearchInput = props => {
  const {onChangeText, value, withFilter, ...restProps} = props;
  const [isFilter, switchFilter] = useState(false);

  return (
    <View style={styles.inputContainer}>
      {withFilter && (
        <TouchableOpacity
          onPress={() => switchFilter(!isFilter)}
          style={styles.imageView}>
          <Image source={isFilter ? Assets.FILTER_ON : Assets.FILTER_OFF} />
        </TouchableOpacity>
      )}
      <View style={styles.inputSubContainer}>
        <Image source={Assets.SEARCH_INPUT} />
        <TextInput
          value={value}
          style={styles.input}
          placeholderTextColor={palette.sky}
          selectionColor={palette.aqua}
          onChangeText={text => onChangeText(text)}
          {...restProps}
        />
        {!!value && (
          <TouchableOpacity onPress={() => onChangeText('')}>
            <Image source={Assets.CLOSE} />
          </TouchableOpacity>
        )}
      </View>
      <TouchableOpacity onPress={() => null}>
        <Image source={Assets.WINDOW_CLOSE} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: palette.aqua,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 10,
    paddingTop: 4,
    paddingBottom: 7,
  },
  inputSubContainer: {
    flexDirection: 'row',
    backgroundColor: palette.navy,
    borderRadius: 20,
    paddingHorizontal: 10,
    height: 35,
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 7,
  },
  input: {
    flex: 1,
    marginLeft: 2,
    marginRight: 5,
    fontSize: 16,
    color: 'white',
    fontFamily: 'Helvetica Neue',
    borderRadius: 20,
    height: 40,
  },
  imageView: {
    minWidth: 20,
    alignItems: 'center',
  },
});

export default SearchInput;
