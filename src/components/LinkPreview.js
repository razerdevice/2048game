import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Linking,
  TouchableOpacity as Touchable,
} from 'react-native';
import PropTypes from 'prop-types';
import urlPropType from 'url-prop-type';
import Assets from '../Assets';
import {palette} from '../theme/variables';
import MyText from './MyText';
import LinkPreview from 'react-native-link-preview';

const Preview = ({link, editMode, close, style}) => {
  const [preview, setPreview] = useState({title: ''});

  const handleClick = async () => {
    try {
      const supported = await Linking.canOpenURL(link);
      if (supported) {
        Linking.openURL(link);
      } else {
        console.log("Don't know how to open URI: " + link);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    const fetch = async () => {
      try {
        const {
          title,
          images: [uri],
        } = await LinkPreview.getPreview(link);
        setPreview({title, uri});
      } catch (e) {
        console.log(e);
      }
    };
    fetch();
  }, [link]);

  const imageStyle = editMode ? styles.imageEdit : styles.image;
  const footerStyle = editMode ? styles.footerEdit : styles.footer;
  const containerStyle = editMode ? styles.containerEdit : styles.container;

  return (
    <View style={[containerStyle, style]}>
      {editMode && (
        <Touchable style={styles.closeIcon} onPress={() => !!close && close()}>
          <Image source={Assets.CLOSE_BLACK} />
        </Touchable>
      )}
      <Image source={{uri: preview.uri}} style={imageStyle} />
      <Touchable style={footerStyle} onPress={handleClick}>
        <MyText style={styles.title} numberOfLines={editMode ? 2 : 1}>
          {preview.title}
        </MyText>
        <MyText style={styles.link} numberOfLines={1}>
          {link.split('/')[2]}
        </MyText>
      </Touchable>
    </View>
  );
};

Preview.propTypes = {
  link: urlPropType.isRequired,
  editMode: PropTypes.bool,
  close: PropTypes.func,
  style: PropTypes.object,
};

export default Preview;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    maxHeight: 300,
    width: '100%',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: palette.lightGray,
    borderRadius: 15,
  },
  containerEdit: {
    height: 100,
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: palette.lightGray,
    borderRadius: 10,
    width: '100%',
    backgroundColor: 'white',
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '75%',
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
  imageEdit: {
    resizeMode: 'cover',
    width: '40%',
    height: '100%',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  link: {
    textAlign: 'left',
    color: palette.grey,
    lineHeight: 24,
    fontSize: 15,
  },
  footer: {
    justifyContent: 'center',
    paddingHorizontal: 12,
    marginBottom: 5,
    height: 55,
  },
  footerEdit: {
    paddingHorizontal: 12,
    width: '60%',
  },
  title: {
    textAlign: 'left',
    color: palette.darkGrey,
    fontSize: 16,
  },
  closeIcon: {
    position: 'absolute',
    top: 7,
    left: 7,
    zIndex: 2,
  },
});
