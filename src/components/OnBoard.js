import React, {Component} from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import {palette} from './../theme/variables';
import Swiper from 'react-native-swiper';
import Button from './Button';
import Strings from './../Strings';
import Assets from './../Assets';
import MyText from './MyText';
import Dot from './SwiperDot';

const {height, width} = Dimensions.get('window');

const isWide = () => width > 360;

export default class OnBoard extends Component {
  renderItem = (text, source) => (
    <View style={styles.slide}>
      <View style={styles.iconBackground}>
        <Image source={source} />
      </View>
      <MyText
        style={
          isWide() ? styles.text : {...styles.text, fontSize: 16, marginTop: 25}
        }>
        {text}
      </MyText>
    </View>
  );

  render() {
    return (
      <View style={styles.wrapper}>
        <Swiper
          containerStyle={styles.containerStyle}
          loop={false}
          dot={<Dot />}
          activeDot={<Dot active />}>
          {this.renderItem(Strings.ONBOARD_1, Assets.SEARCH)}
          {this.renderItem(Strings.ONBOARD_2, Assets.USERS)}
          {this.renderItem(Strings.ONBOARD_3, Assets.STAR)}
        </Swiper>
        <View style={{alignItems: 'center'}}>
          <Button
            color="primary"
            label="JOIN"
            size={isWide() ? 'large' : 'medium'}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: palette.aquaLight,
    paddingVertical: height * 0.08,
  },
  containerStyle: {
    flex: 1,
    marginBottom: 15,
  },
  iconBackground: {
    backgroundColor: palette.aqua,
    width: 145,
    height: 145,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginTop: height * 0.1,
    fontSize: 20,
    lineHeight: 24,
    color: palette.aqua,
    textAlign: 'center',
  },
  slide: {
    paddingTop: height * 0.1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: '15%',
  },
  dot: {
    backgroundColor: palette.aquaMedium,
    width: 16,
    height: 16,
    borderRadius: 8,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 3,
  },
});
