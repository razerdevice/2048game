import _ from 'lodash';
import React, {Fragment, useState} from 'react';
import {StyleSheet, View, TextInput, Image, FlatList} from 'react-native';
import {palette} from './../theme/variables';
import Assets from 'poccketapp/src/Assets';
import ListItem from './ListItem';
import MyText from './MyText';
import {modArray} from '../Utils';

const Autocomplete = props => {
  const {allItems, selected, setSelected, ...restProps} = props;

  const [searchValue, setSearchValue] = useState(' ');

  const renderDivider = () => <View style={styles.divider} />;

  const renderExtraColumn = () => <Image source={Assets.CHECK_MARK} />;

  const getFiltered = () => {
    return allItems.filter(({title}) =>
      title.toLowerCase().includes(searchValue.trim().toLowerCase()),
    );
  };

  const select = id => {
    let selectedIds = modArray(selected, id);
    setSelected(selectedIds);
  };

  const removeLast = () => {
    selected.splice(-1, 1);
    setSelected([...selected]);
    setSearchValue(' ');
  };

  const onChangeText = text => {
    if (_.isEmpty(text) && !_.isEmpty(selected)) {
      return removeLast();
    }
    if (_.isEmpty(text)) return;
    setSearchValue(text);
  };

  const renderSelected = () =>
    selected.map(id => {
      const item = allItems.find(i => i.id === id);
      return (
        <MyText style={styles.text} key={item.id}>
          {`${item.title}, `}
        </MyText>
      );
    });

  return (
    <Fragment>
      <View style={styles.inputContainer}>
        <View style={styles.inputSubContainer}>
          <MyText style={styles.searchTitle}>To: </MyText>

          {renderSelected()}
          <TextInput
            value={searchValue}
            style={styles.input}
            placeholderTextColor={palette.lightGray}
            selectionColor={palette.aqua}
            onChangeText={text => onChangeText(text)}
            {...restProps}
          />
        </View>
      </View>

      <View style={{backgroundColor: 'white'}}>
        <FlatList
          data={searchValue.trim() ? getFiltered() : []}
          renderItem={({item}) => (
            <ListItem
              source={Assets.ENVELOPE}
              title={item.title}
              customColumn={selected.includes(item.id) && renderExtraColumn()}
              onPress={() => select(item.id)}
            />
          )}
          keyExtractor={item => `${item.id}`}
          ItemSeparatorComponent={renderDivider}
        />
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    width: '100%',
  },
  inputSubContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 7,
    paddingVertical: 10,
    borderBottomColor: palette.lightGrey,
    borderBottomWidth: 1,
  },
  input: {
    flex: 1,
    marginLeft: 2,
    marginRight: 5,
    fontSize: 16,
    fontFamily: 'Helvetica Neue',
    borderRadius: 20,
    color: palette.coalGrey,
  },
  imageView: {
    minWidth: 20,
    alignItems: 'center',
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: palette.lightGrey,
  },
  text: {
    color: palette.aqua,
  },
  searchTitle: {
    color: palette.grey,
    marginRight: 5,
    marginLeft: 5,
    fontSize: 16,
  },
});

export default Autocomplete;
