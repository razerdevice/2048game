import React, {Component} from 'react';
import {Dimensions, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {palette} from './../theme/variables';
import PropTypes from 'prop-types';
import MyText from './MyText';

const {height, width} = Dimensions.get('window');

export default class Button extends Component {
  static propTypes = {
    size: PropTypes.oneOf(['small', 'medium', 'large', 'fullWidth']).isRequired,
    color: PropTypes.oneOf(['primary', 'neutral', 'danger', 'inverted'])
      .isRequired,
    label: PropTypes.string,
    outline: PropTypes.bool,
    inverted: PropTypes.bool,
    onPress: PropTypes.func,
  };
  static defaultProps = {
    size: 'medium',
  };

  getSize = () => {
    const {size} = this.props;
    const sizeStyle = styles[`${size}`];
    const textStyle = styles[`${size}Text`];
    return {sizeStyle, textStyle};
  };

  getColor = () => {
    const {color} = this.props;
    let backgroundColor = palette.aqua;
    let fontColor = palette.white;
    switch (color) {
      case 'neutral':
        backgroundColor = palette.lightGrey;
        fontColor = palette.darkGrey;
        break;
      case 'danger':
        backgroundColor = palette.red;
        break;
      case 'inverted':
        backgroundColor = palette.white;
        fontColor = palette.aqua;
    }
    return {backgroundColor, fontColor};
  };

  render() {
    const {outline, label, onPress} = this.props;
    const {sizeStyle, textStyle} = this.getSize();
    const {backgroundColor, fontColor: color} = this.getColor();

    let buttonStyle = [styles.button, sizeStyle, {backgroundColor}];
    let myTextStyle = [textStyle, {color, fontFamily: 'Helvetica Neue'}];

    if (outline) {
      buttonStyle = [...buttonStyle, {borderWidth: 1, borderColor: color}];
    }
    return (
      <TouchableOpacity style={buttonStyle} onPress={onPress}>
        <MyText style={myTextStyle}>{label}</MyText>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 30,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
  small: {
    width: 93,
    height: 35,
  },
  medium: {
    width: 245,
    height: 40,
  },
  large: {
    width: 255,
    height: 50,
  },
  fullWidth: {
    width: width * 0.9,
    height: 45,
  },
  fullWidthText: {
    fontSize: 16,
    color: palette.darkGrey,
  },
  smallText: {
    fontSize: 15,
  },
  mediumText: {
    fontSize: 16,
  },
  largeText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
