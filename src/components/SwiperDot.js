import React from 'react';
import {StyleSheet, View} from 'react-native';
import {palette} from '../theme/variables';

const SwiperDot = ({active, small, color}) => {
  let style = small ? {...styles.dot, ...styles.smallDot} : styles.dot;
  if (color) {
    style = {...style, backgroundColor: color};
  }
  return (
    <View style={active ? [style, {backgroundColor: palette.aqua}] : style} />
  );
};
const styles = StyleSheet.create({
  dot: {
    backgroundColor: palette.aquaMedium,
    width: 16,
    height: 16,
    borderRadius: 8,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 3,
  },
  smallDot: {
    width: 10,
    height: 10,
    marginRight: 3,
  },
});
export default SwiperDot;
