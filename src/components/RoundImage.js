import React from 'react';
import propTypes from 'prop-types';
import urlPropType from 'url-prop-type';
import {Image, StyleSheet} from 'react-native';

const RoundImage = ({uri, outline, size = 'medium', borderWidth = 5}) => {
  const imageStyle = outline
    ? [styles[size], styles.outline, {borderWidth}]
    : styles[size];

  return <Image style={imageStyle} source={{uri}} />;
};

RoundImage.propTypes = {
  uri: urlPropType,
  outline: propTypes.bool,
  size: propTypes.oneOf(['small', 'medium', 'large']),
  borderWidth: propTypes.number,
};

export default RoundImage;

const styles = StyleSheet.create({
  medium: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  large: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  small: {
    width: 30,
    height: 30,
    borderRadius: 50,
  },
  outline: {
    borderColor: 'white',
  },
});
